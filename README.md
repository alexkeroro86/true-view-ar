# Full Pipeline
1. Put the required data in `asset` folder
    ```
    asset
    |-- model
    |-- skybox
    |-- {date}
       |-- data
          |-- trajectory.json
       |-- texture
          |-- rgb_sheet.exr
          |-- depth_sheet.exr
    |-- {date}
    |-- ...
    ```
2. Run `npm install` to install the required packages
3. Run `npm run build` to update the `dist` folder
4. Run `npm run dev` to view on `localhost:9000/`

# Code Structure
* `index.js`: setup GUI and require animation frame
* `Pipeline.js`: control anything related to rendering
* `RealObject.js`: setup for 360-object-capturing object (use GUI `useDepthMesh` to switch billboard or depth mesh mode)
* `VirtualScene.js`: setup for main model
* `util.js`: handle loading processes and definitions

# Demo
* Press __next__ button in GUI to change to the next item
* Press __prev__ button in GUI to change to the previous item