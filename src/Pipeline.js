import * as THREE from 'three';
import { ARButton } from 'three/examples/jsm/webxr/ARButton';
import { VirtualScene } from './VirtualScene';
import { RealObject } from './RealObject';
import { FSM, loadHDRCubemap } from './util';

// import CameraControls from 'camera-controls';
// CameraControls.install({THREE: THREE});


export class Pipeline {
    constructor() {
        // HTML
        this.canvas = null;
        this.context = null;

        // Graphics
        this.width = null;
        this.height = null;
        this.renderer = null;
        this.pmremGenerator = null;
        this.hdrCubeRnederTarget = null;
        this.isRendered = false;

        // Control
        this.camera = null;
        this.cameraControls = null;
        this.cameraWorldPosition = new THREE.Vector3();
        this.state = null;

        // Object
        this.realObjects = [];
        this.virtualScene = null;
        this.mainScene = null;
        this.loaded = false;
        this.whichRealObject = 0;
        this.maxRealObjects = 0;

        // Light
        this.sun = null;
        this.sunTarget = null;
    }

    init(container) {
        // HTML
        this.canvas = document.createElement('canvas');
        this.width = window.innerWidth;
        this.height = window.innerHeight;
        
        // WebGL2
        if (!!(window.WebGL2RenderingContext && this.canvas.getContext('webgl2'))) {
            console.log('Support WebGL2!');
            this.context = this.canvas.getContext('webgl2');
        }
        else {
            this.context = this.canvas.getContext('webgl');
        }
        
        // Graphics
        this.renderer = new THREE.WebGLRenderer({canvas: this.canvas, context: this.context, alpha: true});
        this.renderer.setSize(this.width, this.height);
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.xr.enabled = true;
        container.appendChild(this.renderer.domElement);

        document.body.appendChild(ARButton.createButton(this.renderer));

        // // HDR image-based lighting
        // this.renderer.physicallyCorrectLights = true;
        // this.renderer.toneMapping = THREE.ACESFilmicToneMapping;
        // this.renderer.outputEncoding = THREE.sRGBEncoding;

        // this.pmremGenerator = new THREE.PMREMGenerator(this.renderer);
        // this.pmremGenerator.compileCubemapShader();

        // Shadow mapping
        this.renderer.shadowMap.enabled = true;
        this.renderer.shadowMap.type = THREE.PCFShadowMap;
        
        this.mainScene = new THREE.Scene();

        // Light
        this.sun = new THREE.DirectionalLight('rgb(255,255,255)');
        this.sun.castShadow = true;
        this.sun.position.set(15 + 5, 10, 15 + 5);
        this.sun.target.position.set(5, 0, -5);
        this.sun.shadow.camera.near = 0.1;
        this.sun.shadow.camera.far = 75.0;
        this.sun.shadow.camera.left = -20;
        this.sun.shadow.camera.right = 20;
        this.sun.shadow.camera.top = 20;
        this.sun.shadow.camera.bottom = -20;
        this.sun.shadow.mapSize.width = 2048;
        this.sun.shadow.mapSize.height = 2048;
        this.mainScene.add(this.sun, this.sun.target);
        // this.mainScene.add(new THREE.CameraHelper(this.sun.shadow.camera)); // TEST

        // Object
        // this.virtualScene = new VirtualScene('./asset/model/audi-dealership.glb');
        this.realObjects.push(new RealObject(
            './asset/20201117_141139/',
            new THREE.Vector3(0, 0, 0),
            3
        ));
        // this.realObjects.push(new RealObject(
        //     './asset/20210202_161917/',
        //     new THREE.Vector3(10, 1, -15),
        //     3
        // ));

        // this.realObjects.push(new RealObject(
        //     './asset/20201117_141139/texture/rgb_sheet.png',
        //     './asset/20201117_141139/texture/depth_sheet.exr',
        //     './asset/20201117_141139/data/trajectory.json',
        //     10, 9,
        //     new THREE.Vector3(10, 1, -15)
        // ));

        const gridHelper = new THREE.GridHelper(100, 10);
        gridHelper.frustumCulled = false;
        this.mainScene.add(gridHelper);
        let axesHelper = new THREE.AxesHelper(5);
        this.mainScene.add(axesHelper);

        // Control
        this.camera = new THREE.PerspectiveCamera(60, this.width / this.height, 0.01, 1000);
        // this.camera.position.set(0, 0, 5);
        // this.cameraControls = new CameraControls(this.camera, this.renderer.domElement);
        this.cameraControls = this.renderer.xr.getController(0);
        this.cameraControls.addEventListener('select', this.onSelect.bind(this));
        this.mainScene.add(this.cameraControls);

        this.change(FSM['free']);
        this.loadAll();  // NOT forced to wait
        this.maxRealObjects = this.realObjects.length;
    }

    async loadAll() {
        // let hdrCube = await loadHDRCubemap('./asset/skybox/', ['px.hdr', 'nx.hdr', 'py.hdr', 'ny.hdr', 'pz.hdr', 'nz.hdr'], this.pmremGenerator);
        // this.mainScene.background = hdrCube.map;
        // await this.virtualScene.load();
        // this.virtualScene.setup(hdrCube.renderTarget.texture);
        // this.mainScene.add(this.virtualScene.scene);  // add scene
        for (let realObject of this.realObjects) {
            await realObject.load();
        }

        for (let realObject of this.realObjects) {
            this.mainScene.add(realObject.object);  // add scene
            // let CameraHelper = new THREE.CameraHelper(realObject.virtualCamera);  // TEST
            // this.mainScene.add(CameraHelper);
        }

        // this.change(FSM['3d']);
        this.loaded = true;
        alert('Loading complete!');
    }

    onSelect() {
        this.realObjects[0].object.position.set(0, 0, -this.realObjects[0].radius).applyMatrix4(this.cameraControls.matrixWorld);
        this.realObjects[0].object.quaternion.setFromRotationMatrix(this.cameraControls.matrixWorld);
        this.realObjects[0].anchor.set(
            this.realObjects[0].object.position.x,
            this.realObjects[0].object.position.y,
            this.realObjects[0].object.position.z
        );
    }

    change(state) {
        this.state = state;

        if (this.state == FSM['free']) {
            // this.camera.fov = 60.0;
            // this.camera.updateProjectionMatrix();
            // this.cameraControls.truckSpeed = 2.0;
            // this.cameraControls.minDistance = 0.0;
            // this.cameraControls.maxDistance = Infinity;
            // this.cameraControls.minPolarAngle = 0.0;
            // this.cameraControls.maxPolarAngle = Math.PI;

            // for (let realObject of this.realObjects) {
            //     realObject.change(this.state);
            // }
        }
        else if (this.state == FSM['3d']) {
            // this.realObjects[this.whichRealObject].lookFrom(this.camera, this.cameraControls);
            // this.cameraControls.truckSpeed = 0.0;
            // this.cameraControls.minPolarAngle = this.cameraControls.maxPolarAngle = this.cameraControls.polarAngle;
            
            // this.realObjects[this.whichRealObject].change(this.state);
        }
        else if (this.state == FSM['2d']) {
            // TODO
        }
    }

    // GUI helper
    setTransparent(val) {
        for (let realObject of this.realObjects) {
            realObject.object.material.transparent = val;
            realObject.object.material.needsUpdate = true;
        }
    }

    setUseDepthMesh(val) {
        for (let realObject of this.realObjects) {
            realObject.uniforms.uUseDepthMesh.value = val;
        }
    }

    setSunPosition(x, y, z) {
        this.sun.position.set(x, y, z);
    }

    setSunTarget(x, y, z) {
        this.sun.target.position.set(x, y, z);
    }

    setFaceSunLight(val) {
        for (let realObject of this.realObjects) {
            realObject.uniforms.uFaceSunLight.value = val;
        }
    }

    setPrevRealObject() {
        this.realObjects[this.whichRealObject].object.visible = false;
        this.whichRealObject = (this.whichRealObject - 1 + this.maxRealObjects) % this.maxRealObjects;
        this.realObjects[this.whichRealObject].object.visible = true;
        this.change(FSM['3d']);
    }

    setNextRealObject() {
        this.realObjects[this.whichRealObject].object.visible = false;
        this.whichRealObject = (this.whichRealObject + 1) % this.maxRealObjects;
        this.realObjects[this.whichRealObject].object.visible = true;
        this.change(FSM['3d']);
    }

    update() {
        // TODO: whether can rotate in '3d'
        // const hasControlsUpdated = this.cameraControls.update(delta);
        
        if (!this.loaded) return;
        
        this.cameraWorldPosition.setFromMatrixPosition(this.camera.matrixWorld);
        for (let realObject of this.realObjects) {
            realObject.update(this.cameraWorldPosition, this.sun.position);
        }
    }

    render() {
        if (this.state == FSM['free']) {
            this.renderer.render(this.mainScene, this.camera);
        }
        else if (this.state == FSM['3d']) {
            this.renderer.render(this.mainScene, this.camera);

            // Render all once
            if (this.loaded && !this.isRendered) {
                for (let i in this.realObjects) {
                    if (i == 0) continue;
                    this.realObjects[i].object.visible = false;
                }
                this.isRendered = true;
            }
        }
        else if (this.state == FSM['2d']) {
            // TODO
        }
    }

    resize(width, height) {
        this.camera.aspect = width / height;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(width, height);
    }

    dispose() {
        for (let realObject of this.realObjects) {
            realObject.dispose();
        }
        // this.virtualScene.dispose();
        this.realObjects = [];
        this.virtualScene = null;
    }
}