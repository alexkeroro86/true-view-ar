import * as THREE from 'three';
import * as Dat from 'dat.gui';
import * as Stats from 'stats-js';
import { Pipeline } from './Pipeline';
import { FSM } from './util';


const clock = new THREE.Clock();
var pipeline = null;
var container = null;
var gui = null;
var stats = null;
var params = {
    state: FSM['3d'],
    transparent: true,
    useDepthMesh: true,
    faceSunLight: true,
    sunPosX: 20,
    sunPosY: 10,
    sunPosZ: 20,
    sunTargetX: 5,
    sunTargetY: 0,
    sunTargetZ: -5,
}

function init() {
    container = document.createElement('div');
    document.body.appendChild(container);

    pipeline = new Pipeline();
    pipeline.init(container);

    window.addEventListener('resize', (ev) => {
        pipeline.resize(window.innerWidth, window.innerHeight);
    });
    window.addEventListener('beforeunload', (ev) => {
        pipeline.dispose();
    });

    // stats = new Stats();
    // container.appendChild(stats.dom);

    // gui = new Dat.GUI();
    // let devFolder = gui.addFolder('Dev');
    // devFolder.add(params, 'state', FSM).onChange((val) => {
    //     pipeline.change(val);
    // });
    // devFolder.add(params, 'transparent').onChange((val) => {
    //     pipeline.setTransparent(val);
    // });
    // devFolder.add(params, 'useDepthMesh').onChange((val) => {
    //     pipeline.setUseDepthMesh(val);
    // });
    // let sunFolder = gui.addFolder('Sun');
    // sunFolder.add(params, 'faceSunLight').onChange((val) => {
    //     pipeline.setFaceSunLight(val);
    // });
    // sunFolder.add(params, 'sunPosX', -50, 50, 1).onChange((val) => {
    //     pipeline.setSunPosition(params.sunPosX, params.sunPosY, params.sunPosZ);
    // });
    // sunFolder.add(params, 'sunPosY', -50, 50, 1).onChange((val) => {
    //     pipeline.setSunPosition(params.sunPosX, params.sunPosY, params.sunPosZ);
    // });
    // sunFolder.add(params, 'sunPosZ', -50, 50, 1).onChange((val) => {
    //     pipeline.setSunPosition(params.sunPosX, params.sunPosY, params.sunPosZ);
    // });
    // sunFolder.add(params, 'sunTargetX', -50, 50, 1).onChange((val) => {
    //     pipeline.setSunTarget(params.sunTargetX, params.sunTargetY, params.sunTargetZ);
    // });
    // sunFolder.add(params, 'sunTargetY', -50, 50, 1).onChange((val) => {
    //     pipeline.setSunTarget(params.sunTargetX, params.sunTargetY, params.sunTargetZ);
    // });
    // sunFolder.add(params, 'sunTargetZ', -50, 50, 1).onChange((val) => {
    //     pipeline.setSunTarget(params.sunTargetX, params.sunTargetY, params.sunTargetZ);
    // });
    // let demoFolder = gui.addFolder('Demo');
    // demoFolder.add({'prev': () => { pipeline.setPrevRealObject(); }}, 'prev');
    // demoFolder.add({'next': () => { pipeline.setNextRealObject(); }}, 'next');
    // demoFolder.open();
}

function animate(timestamp, frame) {
    // const delta = clock.getDelta();
    // gui.updateDisplay();
    // stats.update();
    pipeline.update();
    pipeline.render();

    // window.requestAnimationFrame(animate);
}

window.addEventListener('load', (ev) => {
    init();
    // window.requestAnimationFrame(animate);
    pipeline.renderer.setAnimationLoop(animate);
});