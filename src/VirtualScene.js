import * as THREE from 'three';
import { loadGLTF } from './util';


export class VirtualScene {
    constructor(url) {
        this.url = url;
        this.scene = null;
    }

    async load() {
        this.scene = await loadGLTF(this.url);
    }

    setup(envMap) {
        this.scene.traverse((object) => {
            if (object.isMesh) {
                if (envMap !== null) {
                    object.material.envMap = envMap;
                }

                // Update transparent material
                if (object.name == 'SketchUp008' || object.name == 'ID250_1' ) {
                    object.material.transparent = true;
                    object.material.opacity = 0.5;
                    object.material.blendEquation = THREE.AddEquation;
                    object.material.blendSrc = THREE.SrcAlphaFactor;
                    object.material.blendDst = THREE.OneMinusSrcAlphaFactor;
                }
                object.material.needsUpdate = true;
                //

                object.castShadow = false;
                object.receiveShadow = true;
            }
        });
    }

    dispose() {
        this.envMap = null;
        this.scene = null;
    }
}