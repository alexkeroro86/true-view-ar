import * as THREE from 'three';
import { FSM, loadImage, loadEXRImage, loadJSON } from './util';


export class RealObjectOld {
    constructor(texUrl, depthUrl, dataUrl, tileHorz, tileVert, anchor) {
        this.texUrl = texUrl;
        this.depthUrl = depthUrl;
        this.dataUrl = dataUrl;
        this.tileHorz = tileHorz;
        this.tileVert = tileVert;
        this.tileNum = this.tileHorz * this.tileVert;
        this.tileIdx = -1;
        this.tex = null;
        this.depth = null;
        this.views = [];
        this.ground = null;
        this.fov = null;
        this.radius = null;
        this.offsetGround = null;
        this.offsetAll = null;
        this.scale = null;
        this.uniforms = null;
        this.object = null;
        this.anchor = anchor;
        this.state = null;
        this.virtualCamera = new THREE.PerspectiveCamera();
        this.virtualDepthCamera = new THREE.PerspectiveCamera();
        this.depthIdx = -1;
    }

    async load() {
        this.tex = await loadImage(this.texUrl);
        this.tex.wrapS = this.tex.wrapT = THREE.RepeatWrapping;
        this.tex.repeat.set(1.0 / this.tileHorz, 1.0 / this.tileVert);
        this.depth = await loadEXRImage(this.depthUrl);
        this.depth.wrapS = this.depth.wrapT = THREE.RepeatWrapping;
        this.depth.repeat.set(1.0 / this.tileHorz, 1.0 / this.tileVert);

        // Create object
        let geometry = new THREE.PlaneGeometry(16.0/9.0, 1.0, 256, 256);  // for better quality, 256x256
        this.uniforms = {
            uTex: { value: this.tex },
            uDepth: { value: this.depth },
            uConcenticInverseViewMatrix: { value: this.virtualCamera.matrixWorld },
            uUseDepthMesh: { value: true },  // TEST
            uFaceSunLight: { value: true },  // TEST
            uMapRepeat: { value: new THREE.Vector2(1 / this.tileHorz, 1 / this.tileVert) },
            uMapOffset: { value: new THREE.Vector2(0, 0) },
            uFx: { value: -1 },
            uFy: { value: -1 },
            uCx: { value: -1 },
            uCy: { value: -1 },
            uImgw: { value: -1 },
            uImgh: { value: -1 },
            uDepthMapOffset: { value: new THREE.Vector2(0, 0) },
            uDepthConcenticInverseViewMatrix: { value: this.virtualDepthCamera.matrixWorld },          
        };
        let material = new THREE.ShaderMaterial({
            vertexShader: `
            out vec2 vTexcoord;
            uniform float uFx;  // TODO: unifom block
            uniform float uFy;
            uniform float uCx;
            uniform float uCy;
            uniform float uImgw;
            uniform float uImgh;
            uniform int uUseDepthMesh;
            uniform mat4 uConcenticInverseViewMatrix;
            uniform sampler2D uDepth;
            uniform mediump vec2 uMapRepeat;
            uniform mediump vec2 uMapOffset;
            void main() {
                float depth = texture(uDepth, uv * uMapRepeat + uMapOffset).r;
                vec3 vertex = vec3(
                    (uv.x * uImgw - uCx) * depth / uFx,
                    (uv.y * uImgh - uCy) * depth / uFy,
                    -depth);
                vertex = vec3(uConcenticInverseViewMatrix * vec4(vertex, 1.0));

                vTexcoord = uv;
                if (uUseDepthMesh == 1)
                    gl_Position = projectionMatrix * viewMatrix * vec4(vertex, 1.0);
                else
                    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
            }
            `,
            fragmentShader: `
            precision mediump float;
            out vec4 fFragColor;
            in vec2 vTexcoord;
            uniform sampler2D uTex;
            uniform sampler2D uDepth;
            uniform mediump vec2 uMapRepeat;
            uniform mediump vec2 uMapOffset;
            void main() {
                vec4 color = texture(uTex, vTexcoord * uMapRepeat + uMapOffset);
                if (color.a < 0.1) discard;
                fFragColor = color;
            }
            `,
            uniforms: this.uniforms,
            // lights: true,
            transparent: true,
            side: THREE.DoubleSide,
            glslVersion: THREE.GLSL3,
        });
        this.object = new THREE.Mesh(geometry, material);
        this.object.position.set(this.anchor.x, this.anchor.y, this.anchor.z);
        this.object.castShadow = true;
        this.object.receiveShadow = true;
        this.object.customDepthMaterial = new THREE.ShaderMaterial({
            // https://github.com/mrdoob/three.js/blob/dev/src/renderers/shaders/ShaderLib/depth_vert.glsl.js
            vertexShader: `
            // This is used for computing an equivalent of gl_FragCoord.z that is as high precision as possible.
            // Some platforms compute gl_FragCoord at a lower precision which makes the manually computed value better for
            // depth-based postprocessing effects. Reproduced on iPad with A10 processor / iPadOS 13.3.1.
            varying vec2 vHighPrecisionZW;

            // Modify
            varying vec2 vTexcoord;
            uniform float uFx;  // TODO: unifom block
            uniform float uFy;
            uniform float uCx;
            uniform float uCy;
            uniform float uImgw;
            uniform float uImgh;
            uniform int uUseDepthMesh;
            uniform int uFaceSunLight;  // TEST (true is right)
            uniform mat4 uConcenticInverseViewMatrix;  // TEST
            uniform mat4 uDepthConcenticInverseViewMatrix;
            uniform sampler2D uDepth;
            uniform mediump vec2 uMapRepeat;
            uniform mediump vec2 uMapOffset;  // TEST
            uniform mediump vec2 uDepthMapOffset;
            //

            void main() {                
                // Modify
                vTexcoord = uv;
                float depth;
                if (uFaceSunLight == 1)
                    depth = texture(uDepth, uv * uMapRepeat + uDepthMapOffset).r;
                else
                    depth = texture(uDepth, uv * uMapRepeat + uMapOffset).r;
                vec3 vertex = vec3(
                    (uv.x * uImgw - uCx) * depth / uFx,
                    (uv.y * uImgh - uCy) * depth / uFy,
                    -depth);
                if (uFaceSunLight == 1)
                    vertex = vec3(uDepthConcenticInverseViewMatrix * vec4(vertex, 1.0));
                else
                    vertex = vec3(uConcenticInverseViewMatrix * vec4(vertex, 1.0));
                if (uUseDepthMesh == 1)
                    gl_Position = projectionMatrix * viewMatrix * vec4(vertex, 1.0);
                else
                    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
                //

                vHighPrecisionZW = gl_Position.zw;
            }
            `,
            // https://github.com/mrdoob/three.js/blob/dev/src/renderers/shaders/ShaderLib/depth_frag.glsl.js
            fragmentShader: `
            #if DEPTH_PACKING == 3200
                uniform float opacity;
            #endif
            #include <packing>
            varying vec2 vHighPrecisionZW;

            // Modify
            varying vec2 vTexcoord;
            uniform sampler2D uTex;
            uniform int uFaceSunLight;  // TEST (true is right)
            uniform mediump vec2 uMapRepeat;
            uniform mediump vec2 uMapOffset;  // TEST
            uniform mediump vec2 uDepthMapOffset;
            //

            void main() {
                // Modify
                vec4 color;
                if (uFaceSunLight == 1)
                    color = texture(uTex, vTexcoord * uMapRepeat + uDepthMapOffset);
                else
                    color = texture(uTex, vTexcoord * uMapRepeat + uMapOffset);
                if (color.a < 0.1) discard;
                //

                // Higher precision equivalent of gl_FragCoord.z. This assumes depthRange has been left to its default values.
                float fragCoordZ = 0.5 * vHighPrecisionZW[0] / vHighPrecisionZW[1] + 0.5;
                #if DEPTH_PACKING == 3200
                    gl_FragColor = vec4( vec3( 1.0 - fragCoordZ ), opacity );
                #elif DEPTH_PACKING == 3201
                    gl_FragColor = packDepthToRGBA( fragCoordZ );
                #endif
            }
            `,
            uniforms: this.uniforms,
            defines: {
                'DEPTH_PACKING': THREE.RGBADepthPacking,
            },
        });

        let data = await loadJSON(this.dataUrl);
        for (let i in data.camera) {
            this.views.push(new THREE.Vector3(data.camera[i].x, data.camera[i].y, data.camera[i].z));
        }
        this.ground = new THREE.Vector3(data.ground.x, data.ground.y, data.ground.z);
        this.radius = data.radius;
        this.fov = data.fov;
        this.offsetGround = data.offsetGround;
        this.offsetAll = data.offsetAll;
        this.scaleAR = data.scaleAR;
        this.scaleFit = data.scaleFit;

        this.object.scale.set(this.scaleFit, this.scaleFit, this.scaleFit);  // fit captured scale
        // TODO: offset for 2D only (no offset in depth mesh mode since the warped pose looks at the ground of car)
        // this.object.position.y = this.anchor.y + this.offsetAll * this.scaleFit;
        // this.object.position.y = this.anchor.y + this.offsetGround * this.scaleFit;

        this.uniforms.uFx.value = data.fx;
        this.uniforms.uFy.value = data.fy;
        this.uniforms.uCx.value = data.cx;
        this.uniforms.uCy.value = data.cy;
        this.uniforms.uImgw.value = data.imgw;
        this.uniforms.uImgh.value = data.imgh;
    }

    _getFacingAngle(position) {
        let azimuthaAngleDeg = Math.atan2(
            position.x - this.object.position.x,
            position.z - this.object.position.z
        );
        azimuthaAngleDeg = THREE.Math.radToDeg(azimuthaAngleDeg);
        if (azimuthaAngleDeg < 0) azimuthaAngleDeg += 360;
        return azimuthaAngleDeg;
    }

    _getConcentricPosition(i) {
        let from = this.views[i].clone().sub(this.ground);
        from.y = -from.y;
        from = from.add(this.object.position);
        return from;
    }

    lookFrom(camera, cameraControls) {
        // this.virtualCamera.fov = this.fov;
        // this.virtualCamera.updateProjectionMatrix();

        camera.fov = this.fov;
        camera.updateProjectionMatrix();
        let from = this._getConcentricPosition(0);
        let at = this.object.position;
        cameraControls.setLookAt(from.x, from.y, from.z, at.x, at.y, at.z);
        cameraControls.minDistance = cameraControls.distance - this.radius * 0.5;
        cameraControls.maxDistance = cameraControls.distance;
    }

    change(state) {
        this.state = state;
    }

    update(cameraPosition, sunPosition) {
        // Update geometry
        let cameraAzimuthaAngleDeg = this._getFacingAngle(cameraPosition);
        let depthAzimuthaAngleDeg = this._getFacingAngle(sunPosition);

        if (this.state == FSM['free']) {
            this.object.lookAt(this.virtualCamera.position);
        }
        else if (this.state == FSM['3d']) {
            this.object.lookAt(cameraPosition);
        }
        else if (this.state == FSM['2d']) {
            // TODO
        }

        // Update virtual camera
        let circle_y = this._getConcentricPosition(0).y;
        this.virtualCamera.position.set(
            this.anchor.x + this.radius * Math.cos(THREE.MathUtils.degToRad(-cameraAzimuthaAngleDeg + 90)),
            circle_y,
            this.anchor.z + this.radius * Math.sin(THREE.MathUtils.degToRad(-cameraAzimuthaAngleDeg + 90)));
        let at = this.object.position;
        this.virtualCamera.lookAt(at);
        this.virtualCamera.updateMatrixWorld();
        this.uniforms.uConcenticInverseViewMatrix.value = this.virtualCamera.matrixWorld;
        // TODO: duplicated
        this.virtualDepthCamera.position.set(
            this.anchor.x + this.radius * Math.cos(THREE.MathUtils.degToRad(-depthAzimuthaAngleDeg + 90)),
            circle_y,
            this.anchor.z + this.radius * Math.sin(THREE.MathUtils.degToRad(-depthAzimuthaAngleDeg + 90)));
        this.virtualDepthCamera.lookAt(at);
        this.virtualDepthCamera.updateMatrixWorld();
        this.uniforms.uDepthConcenticInverseViewMatrix.value = this.virtualDepthCamera.matrixWorld;
        //
        
        // Update texture
        let currTileIdx = Math.floor(cameraAzimuthaAngleDeg / Math.ceil(360 / this.tileNum));
        if (currTileIdx != this.tileIdx) {
            this.tileIdx = currTileIdx;
            const offsetX = this.tileIdx % this.tileHorz;
            const offsetY =
                this.tileVert -
                1 -
                Math.floor(this.tileIdx / this.tileHorz);

            this.uniforms.uMapOffset.value = new THREE.Vector2(
                offsetX / this.tileHorz,
                offsetY / this.tileVert
            );
        }
        // TODO: duplicated
        let currDepthIdx = Math.floor(depthAzimuthaAngleDeg / Math.ceil(360 / this.tileNum));
        if (currDepthIdx != this.depthIdx) {
            this.depthIdx = currDepthIdx;
            const offsetX = this.depthIdx % this.tileHorz;
            const offsetY =
                this.tileVert -
                1 -
                Math.floor(this.depthIdx / this.tileHorz);

            this.uniforms.uDepthMapOffset.value = new THREE.Vector2(
                offsetX / this.tileHorz,
                offsetY / this.tileVert
            );
        }
        //
    }

    dispose() {
        this.tex = null;
        this.depth = null;
        this.views = [];
        this.uniforms = null;
        this.object = null;
    }
}