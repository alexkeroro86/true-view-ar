import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { HDRCubeTextureLoader } from 'three/examples/jsm/loaders/HDRCubeTextureLoader';
import { EXRLoader } from 'three/examples/jsm/loaders/EXRLoader';


export var FSM = {
    'free': 0,
    '3d': 1,
    '2d': 2,
}

export function loadJSON(url) {
    let fr = new FileReader();
    return new Promise((resolve, reject) => {
        let req = new XMLHttpRequest();
        req.open('GET', url, true);
        req.responseType = 'blob';
        req.onload = () => {
            fr.readAsText(req.response);
            fr.onload = (e) => {
                let f = e.target.result;
                // let ls = f.split(/\r?\n/);
                let ls = JSON.parse(f);
                resolve(ls);
            };
        }
        req.onerror = () => {
            reject(new Error(`Failed to load file's url: ${url}`));
        };
        req.send();
    });
}

export function loadImage(url) {
    let loader = new THREE.TextureLoader();
    return new Promise((resolve, reject) => {
        loader.load(
            url,
            (ret) => {
                resolve(ret);
            },
            (xhr) => {
                // console.log((xhr.loaded / xhr.total * 100) + '% loaded');
            },
            (error) => {
                reject(error);
            }
        );
    });
}

// MUST be 4-channel RGBA
export function loadEXRImage(url) {
    let loader = new EXRLoader();
    return new Promise((resolve, reject) => {
        loader.setDataType(THREE.FloatType).load(
            url,
            (ret) => {
                // ret.format = THREE.RedFormat;
                // ret.internalFormat = 'R32F';
                // ret.needsUpdate = true;
                resolve(ret);
            },
            (xhr) => {

            },
            (error) => {
                reject(error);
            }
        );
    });
}

export function loadHDRCubemap(rootUrl, urls, pmremGenerator) {
    return new Promise((resolve, reject) => {
        let hdrCubemap = new HDRCubeTextureLoader()
            .setPath(rootUrl)
            .setDataType(THREE.UnsignedByteType)
            .load(urls, () => {
                let hdrCubeRenderTarget = pmremGenerator.fromCubemap(hdrCubemap)
                hdrCubemap.magFilter = THREE.LinearFilter;
                hdrCubemap.needsUpdate = true;
                resolve({map: hdrCubemap, renderTarget: hdrCubeRenderTarget});
            },
            (xhr) => {
                
            },
            (error) => {
                reject(error);
            });
    })
}

export function loadGLTF(url) {
    let loader = new GLTFLoader();
    return new Promise((resolve, reject) => {
        loader.load(
            url,
            (gltf) => {
                resolve(gltf.scene || gltf.scenes[0]);
            },
            (xhr) => {
                // console.log((xhr.loaded / xhr.total * 100) + '% loaded');
            },
            (error) => {
                reject(error);
            }
        );
    });
}