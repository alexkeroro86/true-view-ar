import * as THREE from 'three';
import { FSM, loadImage, loadEXRImage, loadJSON } from './util';

export class RealObject {
    constructor(rootUrl, anchor, halfNumBuffer) {
        // JSON
        this.rootUrl = rootUrl;
        this.views = []
        this.ground = null;
        this.radius = null;
        this.fov = null;
        this.total = null;
        this.angle = null;
        this.numCol = null;
        this.numRow = null;
        this.rgbUrls = []
        this.depthUrls = []
        this.rgbSheets = []
        this.depthSheets = []

        // Shader
        this.uniforms = null;
        this.rgbIdx = -1;
        this.shadowIdx = -1;
        this.rgbSheetIdx = -1;
        this.shadowSheetIdx = -1;

        // Member
        this.state = null;
        this.object = null;
        this.anchor = anchor;
        this.forward = null;
        this.cameraHeight = 0.0;
        this.initAngle = 0.0;  // global initial rotation is then added by local rotation
        this.virtualCamera = new THREE.PerspectiveCamera();
        this.virtualShadowCamera = new THREE.PerspectiveCamera();  // for shadow
        this.rgbSheetReady = true;  // 
        this.shadowSheetReady = false;
        this.rgbSheetLoading = [];
        this.depthSheetLoading = [];
        this.halfNumBuffer = halfNumBuffer;
    }

    async load() {
        // Load JSON (change OpenCV to Three.js coordinate system by flipping y and z)
        let data = await loadJSON(this.rootUrl + 'trajectory.json');
        for (let i in data.camera) {
            this.views.push(new THREE.Vector3(data.camera[i].x, -data.camera[i].y, -data.camera[i].z));
        }
        this.ground = new THREE.Vector3(data.ground.x, -data.ground.y, -data.ground.z);
        this.radius = data.radius;
        this.fov = data.fov;
        this.total = data.total;
        this.angle = Math.ceil(360 / this.total);

        // Load texture
        this.numCol = data.col;
        this.numRow = data.row;
        for (let i in data.rgb_path) {
            this.rgbUrls.push(this.rootUrl + data.rgb_path[i]);
            this.depthUrls.push(this.rootUrl + data.depth_path[i]);
            this.rgbSheets.push(null);
            this.depthSheets.push(null);
            this.rgbSheetLoading.push(false);
            this.depthSheetLoading.push(false);
        }

        // Color shader
        // let material = new THREE.MeshBasicMaterial({ color: 0xcecece });
        this.uniforms = {
            uTex: { value: null },
            uDepth: { value: null },
            uConcenticInverseViewMatrix: { value: this.virtualCamera.matrixWorld },
            uUseDepthMesh: { value: true },  // TEST
            uFaceSunLight: { value: true },  // TEST
            uMapRepeat: { value: new THREE.Vector2(1.0 / this.numCol, 1.0) },
            uMapOffset: { value: new THREE.Vector2(0, 0) },
            uFx: { value: data.fx },
            uFy: { value: data.fy },
            uCx: { value: data.cx },
            uCy: { value: data.cy },
            uImgw: { value: data.imgw },
            uImgh: { value: data.imgh },
            uShadowTex: { value: null },
            uShadowDepth: { value: null },
            uShadowMapOffset: { value: new THREE.Vector2(0, 0) },
            uShadowConcenticInverseViewMatrix: { value: this.virtualShadowCamera.matrixWorld },          
        };
        let material = new THREE.ShaderMaterial({
            vertexShader: `
            out vec2 vTexcoord;
            uniform float uFx;  // TODO: unifom block
            uniform float uFy;
            uniform float uCx;
            uniform float uCy;
            uniform float uImgw;
            uniform float uImgh;
            uniform int uUseDepthMesh;
            uniform mat4 uConcenticInverseViewMatrix;
            uniform sampler2D uDepth;
            uniform mediump vec2 uMapRepeat;
            uniform mediump vec2 uMapOffset;

            // uniform mat4 uShadowConcenticInverseViewMatrix;  // TEST
            // uniform sampler2D uShadowDepth;
            // uniform mediump vec2 uShadowMapOffset;
            void main() {
                float depth = pow(texture(uDepth, uv * uMapRepeat + uMapOffset).r, 0.5f) * 8.0f;
                vec3 vertex = vec3(
                    (uv.x * uImgw - uCx) * depth / uFx,
                    (uv.y * uImgh - uCy) * depth / uFy,
                    -depth);
                vertex = vec3(uConcenticInverseViewMatrix * vec4(vertex, 1.0));

                // float depth = texture(uShadowDepth, uv * uMapRepeat + uShadowMapOffset).r;
                // vec3 vertex = vec3(
                //     (uv.x * uImgw - uCx) * depth / uFx,
                //     (uv.y * uImgh - uCy) * depth / uFy,
                //     -depth);
                // vertex = vec3(uShadowConcenticInverseViewMatrix * vec4(vertex, 1.0));

                vTexcoord = uv;
                if (uUseDepthMesh == 1)
                    gl_Position = projectionMatrix * viewMatrix * vec4(vertex, 1.0);
                else
                    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
            }
            `,
            fragmentShader: `
            precision mediump float;
            out vec4 fFragColor;
            in vec2 vTexcoord;
            uniform sampler2D uTex;
            uniform sampler2D uDepth;
            uniform mediump vec2 uMapRepeat;
            uniform mediump vec2 uMapOffset;

            // uniform sampler2D uShadowTex;  // TEST
            // uniform mediump vec2 uShadowMapOffset;
            void main() {
                vec4 color = texture(uTex, vTexcoord * uMapRepeat + uMapOffset);
                // vec4 color = texture(uShadowTex, vTexcoord * uMapRepeat + uShadowMapOffset);

                if (color.a < 0.1) discard;
                fFragColor = color;
            }
            `,
            uniforms: this.uniforms,
            transparent: true,
            side: THREE.DoubleSide,
            glslVersion: THREE.GLSL3,
        });
        // Create object
        let geometry = new THREE.PlaneGeometry(16.0 / 9.0, 1.0, 256, 256);
        this.object = new THREE.Mesh(geometry, material);
        this.object.position.set(this.anchor.x, this.anchor.y, this.anchor.z);
        this.object.castShadow = true;
        this.object.receiveShadow = true;
        this.object.scale.set(data.scale, data.scale, data.scale);

        // Depth shader
        this.object.customDepthMaterial = new THREE.ShaderMaterial({
            // https://github.com/mrdoob/three.js/blob/dev/src/renderers/shaders/ShaderLib/depth_vert.glsl.js
            vertexShader: `
            // This is used for computing an equivalent of gl_FragCoord.z that is as high precision as possible.
            // Some platforms compute gl_FragCoord at a lower precision which makes the manually computed value better for
            // depth-based postprocessing effects. Reproduced on iPad with A10 processor / iPadOS 13.3.1.
            varying vec2 vHighPrecisionZW;

            // Modify
            varying vec2 vTexcoord;
            uniform float uFx;  // TODO: unifom block
            uniform float uFy;
            uniform float uCx;
            uniform float uCy;
            uniform float uImgw;
            uniform float uImgh;
            uniform int uUseDepthMesh;
            uniform int uFaceSunLight;  // TEST (true is right)
            uniform mat4 uConcenticInverseViewMatrix;  // TEST
            uniform mat4 uShadowConcenticInverseViewMatrix;
            uniform sampler2D uShadowDepth;
            uniform mediump vec2 uMapRepeat;
            uniform mediump vec2 uMapOffset;  // TEST
            uniform mediump vec2 uShadowMapOffset;
            //

            void main() {                
                // Modify
                vTexcoord = uv;
                float depth;
                if (uFaceSunLight == 1)
                    depth = pow(texture(uShadowDepth, uv * uMapRepeat + uShadowMapOffset).r, 0.5f) * 8.0f;
                else
                    depth = pow(texture(uShadowDepth, uv * uMapRepeat + uMapOffset).r, 0.5f) * 8.0f;
                vec3 vertex = vec3(
                    (uv.x * uImgw - uCx) * depth / uFx,
                    (uv.y * uImgh - uCy) * depth / uFy,
                    -depth);
                if (uFaceSunLight == 1)
                    vertex = vec3(uShadowConcenticInverseViewMatrix * vec4(vertex, 1.0));
                else
                    vertex = vec3(uConcenticInverseViewMatrix * vec4(vertex, 1.0));
                if (uUseDepthMesh == 1)
                    gl_Position = projectionMatrix * viewMatrix * vec4(vertex, 1.0);
                else
                    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
                //

                vHighPrecisionZW = gl_Position.zw;
            }
            `,
            // https://github.com/mrdoob/three.js/blob/dev/src/renderers/shaders/ShaderLib/depth_frag.glsl.js
            fragmentShader: `
            #if DEPTH_PACKING == 3200
                uniform float opacity;
            #endif
            #include <packing>
            varying vec2 vHighPrecisionZW;

            // Modify
            varying vec2 vTexcoord;
            uniform sampler2D uShadowTex;
            uniform int uFaceSunLight;  // TEST (true is right)
            uniform mediump vec2 uMapRepeat;
            uniform mediump vec2 uMapOffset;  // TEST
            uniform mediump vec2 uShadowMapOffset;
            //

            void main() {
                // Modify
                vec4 color;
                if (uFaceSunLight == 1)
                    color = texture(uShadowTex, vTexcoord * uMapRepeat + uShadowMapOffset);
                else
                    color = texture(uShadowTex, vTexcoord * uMapRepeat + uMapOffset);
                if (color.a < 0.1) discard;
                //

                // Higher precision equivalent of gl_FragCoord.z. This assumes depthRange has been left to its default values.
                float fragCoordZ = 0.5 * vHighPrecisionZW[0] / vHighPrecisionZW[1] + 0.5;
                #if DEPTH_PACKING == 3200
                    gl_FragColor = vec4( vec3( 1.0 - fragCoordZ ), opacity );
                #elif DEPTH_PACKING == 3201
                    gl_FragColor = packDepthToRGBA( fragCoordZ );
                #endif
            }
            `,
            uniforms: this.uniforms,
            defines: {
                'DEPTH_PACKING': THREE.RGBADepthPacking,
            },
        });

        // Setup member
        this.forward = this._getLocalPosition(0)
        this.forward.y = 0.0;  // only xz-plane
        this.forward = this.forward.normalize();
    }

    _getGlobalRotation(position) {
        let azimuthaAngleDeg = Math.atan2(
            position.x - this.object.position.x,
            position.z - this.object.position.z
        );
        azimuthaAngleDeg = THREE.Math.radToDeg(azimuthaAngleDeg);
        if (azimuthaAngleDeg < 0) azimuthaAngleDeg += 360;
        return azimuthaAngleDeg;
    }

    _getLocalRotation(position) {  // get local rotation (same as python)
        let direction = position.clone().sub(this.object.position);
        direction.y = 0.0;
        direction = direction.normalize();
        let sign = Math.sign(direction.clone().cross(this.forward).dot(new THREE.Vector3(0.0, 1.0, 0.0)));
        let product = direction.clone().dot(this.forward);
        let radian = Math.acos(THREE.MathUtils.clamp(product, -1.0, 1.0));
        return THREE.MathUtils.radToDeg(sign > 0.0 ? 2.0 * Math.PI - radian : radian) % 360;  // avoid 360 == 0
    }

    _getLocalPosition(i) {  // get local position
        let from = this.views[i].clone().sub(this.ground);
        return from;
    }

    lookFrom(camera, cameraControls) {  // set the first view
        // this.virtualCamera.fov = this.fov;
        // this.virtualCamera.updateProjectionMatrix();

        camera.fov = this.fov;
        camera.updateProjectionMatrix();
        let from = this._getLocalPosition(0);
        from = from.add(this.object.position);
        let at = this.object.position;
        this.cameraHeight = from.y;
        this.initAngle = this._getGlobalRotation(from);
        cameraControls.setLookAt(from.x, from.y, from.z, at.x, at.y, at.z);
        // cameraControls.setLookAt(10 + 2, 6, -15 + 2, at.x, at.y, at.z);
        cameraControls.minDistance = cameraControls.distance - this.radius * 0.5;  // restrict zooming
        cameraControls.maxDistance = cameraControls.distance;
    }

    _updateVirtualCamera(camera, angleDeg) {
        let angle = -(this.initAngle + angleDeg) + 90;
        camera.position.set(
            this.anchor.x + this.radius * Math.cos(THREE.MathUtils.degToRad(angle)),
            this.cameraHeight,
            this.anchor.z + this.radius * Math.sin(THREE.MathUtils.degToRad(angle))
        );
        let at = this.object.position;
        camera.lookAt(at);
        camera.updateMatrixWorld();
    }

    change(state) {
        this.state = state;
    }

    _loadColor(idx) {
        if (this.rgbSheetLoading[idx] == true) return;  // avoid loading duplicated things
        this.rgbSheetLoading[idx] = true;
        loadImage(this.rgbUrls[idx])
            .then((ret) => {
                this.rgbSheets[idx] = ret;
                this.rgbSheets[idx].wrapS = this.rgbSheets[idx].wrapT = THREE.RepeatWrapping;
                this.rgbSheets[idx].repeat.set(1.0 / this.tileCol, 1.0);
                console.log(`Load color complete: ${idx}`);
            })
            .catch(console.error);
        console.log('Loading color');
    }

    _loadDepth(idx) {
        if (this.depthSheetLoading[idx] == true) return;  // avoid loading duplicated things
        this.depthSheetLoading[idx] = true;
        loadImage(this.depthUrls[idx])
            .then((ret) => {
                this.depthSheets[idx] = ret;
                this.depthSheets[idx].wrapS = this.depthSheets[idx].wrapT = THREE.RepeatWrapping;
                this.depthSheets[idx].repeat.set(1.0 / this.tileCol, 1.0);
                console.log(`Load depth complete: ${idx}`);
            })
            .catch(console.error);
        console.log('Loading depth');
    }

    _discardBefore(idx) {
        let rightIdx = (idx + this.halfNumBuffer + 1) % this.numRow;
        let leftIdx = (idx - this.halfNumBuffer - 1 + this.numRow) % this.numRow;
        if (rightIdx != this.shadowSheetIdx && this.rgbSheets[rightIdx] != null) {
            this.rgbSheets[rightIdx] = null;
            this.rgbSheetLoading[rightIdx] = false;
            console.log(`Discard sheet ${rightIdx}`);
        }
        if (leftIdx != this.shadowSheetIdx && this.rgbSheets[leftIdx] != null) {
            this.rgbSheets[leftIdx] = null;
            this.rgbSheetLoading[leftIdx] = false;
            console.log(`Discard sheet ${leftIdx}`);
        }

    }

    update(cameraPosition, sunPosition) {
        // Update angle
        let cameraAzimuthaAngleDeg = this._getLocalRotation(cameraPosition);
        let shadowDepthAzimuthaAngleDeg = this._getLocalRotation(sunPosition);

        // // Update camera
        // this._updateVirtualCamera(this.virtualCamera, cameraAzimuthaAngleDeg);
        // this.uniforms.uConcenticInverseViewMatrix.value = this.virtualCamera.matrixWorld;
        // this._updateVirtualCamera(this.virtualShadowCamera, shadowDepthAzimuthaAngleDeg);
        // this.uniforms.uShadowConcenticInverseViewMatrix.value = this.virtualShadowCamera.matrixWorld;
        
        // // Update object
        // this.object.lookAt(this.virtualCamera.position);

        // Update texture
        //// Color
        let currRGBIdx = Math.floor(cameraAzimuthaAngleDeg / this.angle);
        if (this.rgbIdx == -1) {
            let idx = Math.floor(currRGBIdx / this.numCol);
            this._loadColor(idx);
            this._loadDepth(idx);
        }
        if (currRGBIdx != this.rgbIdx) {
            // Update camera
            this._updateVirtualCamera(this.virtualCamera, cameraAzimuthaAngleDeg);
            this.uniforms.uConcenticInverseViewMatrix.value = this.virtualCamera.matrixWorld;
            this._updateVirtualCamera(this.virtualShadowCamera, shadowDepthAzimuthaAngleDeg);
            this.uniforms.uShadowConcenticInverseViewMatrix.value = this.virtualShadowCamera.matrixWorld;
            
            // Update object
            this.object.lookAt(this.virtualCamera.position);

            this.rgbIdx = currRGBIdx;

            let currRGBSheetIdx = Math.floor(this.rgbIdx / this.numCol);
            if (currRGBSheetIdx != this.rgbSheetIdx) {
                this.rgbSheetIdx = currRGBSheetIdx;
                if (this.rgbSheets[this.rgbSheetIdx] != null && this.depthSheets[this.rgbSheetIdx] != null) {
                    this.uniforms.uTex.value = this.rgbSheets[this.rgbSheetIdx];
                    this.uniforms.uDepth.value = this.depthSheets[this.rgbSheetIdx];
                    this.rgbSheetReady = true;

                    this._discardBefore(this.rgbSheetIdx);
                }
                else {
                    this.rgbSheetReady = false;

                    // Load JIT
                    this._loadColor(currRGBSheetIdx);
                    this._loadDepth(currRGBSheetIdx);

                    console.log(`Wait color sheet: ${this.rgbSheetIdx}`);
                }

                // Pre-load N sheet(s)
                for (let i of [...Array(this.halfNumBuffer).keys()]) {
                    let rightSheetIdx = (currRGBSheetIdx + i + 1) % this.numRow;
                    let leftSheetIdx = (currRGBSheetIdx - i - 1 + this.numRow) % this.numRow;
                    this._loadColor(rightSheetIdx);
                    this._loadColor(leftSheetIdx);
                    this._loadDepth(rightSheetIdx);
                    this._loadDepth(leftSheetIdx);
                }
            }

            const offsetX = this.rgbIdx % this.numCol;
            this.uniforms.uMapOffset.value = new THREE.Vector2(
                offsetX / this.numCol,
                0.0
            );

            console.log(this.rgbIdx, this.rgbSheetIdx, offsetX);
        }
        //// Shadow
        let currShadowIdx = Math.floor(shadowDepthAzimuthaAngleDeg / this.angle);
        if (this.shadowIdx == -1) {
            let idx = Math.floor(currShadowIdx / this.numCol);
            this._loadColor(idx);
            this._loadDepth(idx);
        }
        if (currShadowIdx != this.shadowIdx) {
            this.shadowIdx = currShadowIdx;

            let currShadowSheetIdx = Math.floor(this.shadowIdx / this.numCol);
            if (currShadowSheetIdx != this.shadowSheetIdx) {
                this.shadowSheetIdx = currShadowSheetIdx;
                if (this.rgbSheetIdx[this.shadowSheetIdx] != null && this.depthSheets[this.shadowSheetIdx] != null) {
                    this.uniforms.uShadowTex.value = this.rgbSheetIdx[this.shadowSheetIdx];
                    this.uniforms.uShadowDepth.value = this.depthSheets[this.shadowSheetIdx];
                    this.shadowSheetReady = true;
                }
                else {
                    this.shadowSheetReady = false;
                    
                    // Load JIT
                    this._loadColor(currShadowSheetIdx);
                    this._loadDepth(currShadowSheetIdx);
                    
                    console.log(`Wait shadow sheet: ${this.shadowSheetIdx}`);
                }
            }

            const offsetX = this.shadowIdx % this.numCol;
            this.uniforms.uShadowMapOffset.value = new THREE.Vector2(
                offsetX / this.numCol,
                0.0
            );

            console.log(this.shadowIdx, this.shadowSheetIdx, offsetX);
        }
        
        //// Ready or not
        if (this.rgbSheetReady == false && this.rgbSheets[this.rgbSheetIdx] != null &&
            this.depthSheets[this.rgbSheetIdx] != null) {
            this.uniforms.uTex.value = this.rgbSheets[this.rgbSheetIdx];
            this.uniforms.uDepth.value = this.depthSheets[this.rgbSheetIdx];
            this.rgbSheetReady = true;
            
            this._discardBefore(this.rgbSheetIdx);

            console.log(`Ready color sheet: ${this.rgbSheetIdx}`);
        }
        if (this.shadowSheetReady == false && this.rgbSheets[this.shadowSheetIdx] != null &&
            this.depthSheets[this.shadowSheetIdx] != null) {
            this.uniforms.uShadowTex.value = this.rgbSheets[this.shadowSheetIdx];
            this.uniforms.uShadowDepth.value = this.depthSheets[this.shadowSheetIdx];
            this.shadowSheetReady = true;
            
            console.log(`Ready shadow sheet: ${this.shadowSheetIdx}`);
        }

        // // Mode
        // if (this.state == FSM['free'] || this.state == FSM['3d']) {
        //     this.object.lookAt(this.virtualCamera.position);
        // }
        // else if (this.state == FSM['2d']) {
        //     // TODO
        // }
    }

    dispose() {
        this.rgbUrls = [];
        this.depthUrls = [];
        this.rgbSheets = [];
        this.depthSheets = [];
        this.views = [];
        this.uniforms = null;
        this.object = null;
    }
}